var http = require('http')
var util = require('util');
var cronJob = require('cron').CronJob;
// var mysql = require('mysql');
var nano = require('nano')('http://localhost:5984');
var pubnub = require('pubnub').init({
  publish_key: 'pub-c-4664659f-7da9-45c8-8655-46c8a77181d0',
  subscribe_key: 'sub-c-57aa8f92-6812-11e2-903d-12313f022c90'
});

nano.db.create('readings');
var readings = nano.db.use('readings');

// var connection = mysql.createConnection({
//   host: 'localhost',
//   user: 'monitor',
//   password: 'joepiedepoepie',
//   database: 'jmonitor'
// });

//connection.connect();

var options = {
  host: 'sereta.dyndns.org',
  port: 8081,
  method: 'GET'
};

var lastReading = {};

exports.poll = function() {
  new cronJob('*/30 * * * * *', function() {
    console.log('polling');
    var req = http.request(options, function(res) {
      console.log('STATUS: ' + res.statusCode);
      console.log('HEADERS: ' + JSON.stringify(res.headers));
      res.setEncoding('utf8');
      res.on('data', function(chunk) {
        console.log('BODY: ' + chunk);
        //console.log('last: ' + util.inspect(lastReading,false,null))

        var objReading = JSON.parse(chunk);
        var now = new Date();

        readings.insert({
          'data': objReading,
          'datetime': now
        }, null, function(err, body, header) {
          if(err) {
            console.log('[readings.insert] ', err.message);
            return;
          }
          console.log(now.valueOf());
        });

        pubnub.publish({
          channel: "heating",
          message: { 'key' : now.valueOf(), value : [objReading]},
          callback: function(info) {
            console.log('pubnub: ' + info);
          }
        });

        lastReading = objReading;
      });
    });

    // write data to request body
    req.write('data\n');
    req.write('data\n');
    req.end();

    req.on('error', function(e) {
      console.log('problem with request: ' + e.message);
    });
  }, null, true, "Europe/Madrid");
}