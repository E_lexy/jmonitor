/*
 * Serve JSON to our AngularJS client
 */
var nano = require('nano')('http://localhost:5984')
nano.db.create('readings');
var readings = nano.db.use('readings');
var moment = require('moment');

var mysql = require('mysql');

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'monitor',
  password: 'joepiedepoepie',
  database: 'jmonitor'
});

connection.connect();

exports.readings = function(req, res) {
  var query = connection.query('SELECT * FROM readings WHERE property=\'' + req.params.id + '\' AND created_at > SUBDATE(NOW(), INTERVAL ' + req.params.hr + ' HOUR)', function(err, rows) {
    res.send(rows);
  });
  console.log(query.sql)
};

exports.history = function(req, res) {
  process.env.TZ = 'Europe/Madrid';
  var query = connection.query('SELECT * FROM readings WHERE created_at > SUBDATE(NOW(), INTERVAL ' + req.params.hr + ' HOUR)', function(err, rows) {
    res.send(rows);
  });
  console.log(query.sql)
};

exports.cdb = function(req, res) {
  var start = moment().subtract('hours', parseInt(req.params.hr)+1).valueOf();
  readings.view('list', 'All', {
    startkey: start
  }, function(err, body) {
    if(!err) {
      // body.rows.forEach(function(doc) {
      //   console.log(doc.key);
      // });
      res.send(body);
    } else {
      console.log(err);
    }
  });
};