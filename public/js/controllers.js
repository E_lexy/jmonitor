'use strict';

/* Controllers */

function AppCtrl($scope, pubNub) {
  $scope.latest = 0;
  pubNub.subscribe({
    channel: "heating",
    message: function(m) {
      $scope.latest = m;
      $scope.$apply();
    }
  });
}
MyCtrl1.$inject = ['$scope', 'pubNub'];

function MyCtrl1($scope) {
  // $scope.latest = 0;
  // $scope.$on('heatingPacket', function(event, packet) {
  //   $scope.latest = packet;
  // })  
}
MyCtrl1.$inject = ['$scope', 'pubNub'];

function MyCtrl2($scope, $http, $routeParams) {
  $http({
    method: 'GET',
    url: '/api/cdb/' + ($routeParams.hours ? $routeParams.hours : 2)
  }).
  success(function(data, status, headers, config) {
    $scope.readings = data.rows;
  }).
  error(function(data, status, headers, config) {
    $scope.name = 'Error!'
  });

  // $scope.$on('heatingPacket', function(event, packet) {
  //   $scope.latest = packet;
  // })  
}
MyCtrl2.$inject = ['$scope', '$http', '$routeParams', 'pubNub'];