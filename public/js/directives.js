'use strict';

/* Directives */


angular.module('myApp.directives', []).
directive('appVersion', ['version', function(version) {
  return function(scope, elm, attrs) {
    elm.text(version);
  };
}]).directive('chart', function() {
  return {
    restrict: 'A',
    scope: {
      readings: '=',
      latest: '='
    },
    replace: true,

    controller: function($scope, $element, $attrs) {
      console.log(2);

    },
    template: '<div id="container" style="margin: 0 auto">not working</div>',
    link: function(scope, element, attrs) {
      console.log(3);
      var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'container',
          zoomType: 'x',
          spacingRight: 20,
          alignTicks: false
        },
        title: {
          text: 'Heating overview SeretaLabs, Camarles'
        },
        tooltip: {
          shared: true
        },
        plotOptions: {
          series: {
            marker: {
              enabled: false
            }
          }
        },
        yAxis: [{ // Primary yAxis
          height: 200,
          labels: {
            formatter: function() {
              return this.value + '°C';
            }
          },
          title: {
            text: 'Temperature',
          }
        }, { // Secondary yAxis
          title: {
            text: 'Pump on/off',
            style: {
              color: '#4572A7'
            }
          },
          labels: {
            style: {
              color: '#4572A7'
            }
          },
          top: 270,
          height: 50,
          offset: 1,
          lineWidth: 2,
          max: 1,
          min: 0
          //opposite: true
        }],
        xAxis: {
          type: 'datetime',
          maxZoom: 60,
          title: {
            text: null
          }
        },
        legend: {
          // layout: 'vertical',
          // align: 'left',
          // x: 80,
          verticalAlign: 'top',
          y: 20,
          floating: true
        },
        series: [{
          type: 'spline',
          name: 'Top Tank',
          data: null,
        }, {
          type: 'spline',
          name: 'After heater',
          data: null,
        }, {
          type: 'spline',
          name: 'Panel out',
          data: null,
        }, {
          type: 'spline',
          name: 'Panel in',
          data: null,
        }, {
          type: 'spline',
          name: 'Temp Outside',
          data: null,
        }, {
          type: 'spline',
          name: 'Xchanger out',
          data: null,
        }, {
          type: 'spline',
          name: 'Floor in',
          data: null,
        }, {
          name: 'Gas heater',
          //color: '#4572A7',
          type: 'line',
          yAxis: 1,
          data: null
        }, {
          name: 'Floorpump',
          //color: '#4572A7',
          type: 'line',
          yAxis: 1,
          data: null
        }, {
          name: 'Solarpump',
          //color: '#4572A7',
          type: 'line',
          yAxis: 1,
          data: null

        }, ]
      });
      scope.$watch("readings", function(values) {
        var series = {};
        var arrBool = ['sp', 'fp', 'hp']
        angular.forEach(values, function(data, key) {
          var date = data.key;
          angular.forEach(data.value[0], function(value, key) {
            if(!Array.isArray(this[key])) this[key] = [];
            var divider = arrBool.indexOf(key) !== -1 ? 1 : 10;
            this[key].push([date, value / divider]);
            var tes;
          }, series);
        }, series);
        chart.series[0].setData(series.tt);
        chart.series[1].setData(series.ah);
        chart.series[2].setData(series.po);
        chart.series[3].setData(series.pi);
        chart.series[4].setData(series.pa);
        chart.series[5].setData(series.xo);
        chart.series[6].setData(series.fi);
        chart.series[7].setData(series.hp);
        chart.series[8].setData(series.fp);
        chart.series[9].setData(series.sp, true);

      }, true);

      scope.$watch("latest", function(reading) {
        if(reading) {
          var series = {};
          var arrBool = ['sp', 'fp', 'hp']

          var date = reading.key;
          angular.forEach(reading.value[0], function(value, key) {
            if(!Array.isArray(this[key])) this[key] = [];
            var divider = arrBool.indexOf(key) !== -1 ? 1 : 10;
            this[key] = [date, value / divider];
            var tes;
          }, series);

          chart.series[0].addPoint(series.tt, false, true);
          chart.series[1].addPoint(series.ah, false, true);
          chart.series[2].addPoint(series.po, false, true);
          chart.series[3].addPoint(series.pi, false, true);
          chart.series[4].addPoint(series.pa, false, true);
          chart.series[5].addPoint(series.xo, false, true);
          chart.series[6].addPoint(series.fi, false, true);
          chart.series[7].addPoint(series.hp, false, true);
          chart.series[8].addPoint(series.fp, false, true);
          chart.series[9].addPoint(series.sp, false, true);
          chart.redraw();
        }

      }, true);
    }
  }
}).directive('gauge', function() {
  return {
    restrict: 'A',
    scope: {
      width: '@',
      height: '@',
      max: '@',
      animspeed: '@',
      id: '@',
      value: '='
    },
    //replace: true,

    template: '<canvas width="{{width}}" height="{{height}}"></canvas><div id="gauge-textfield" style="font-size: 24px; ">{{value/10+"&deg"}}</div>',
    link: function(scope, element, attrs) {
      var opts = {
        lines: 12, // The number of lines to draw
        angle: 0.15, // The length of each line
        lineWidth: 0.06, // The line thickness
        pointer: {
          length: 0.9, // The radius of the inner circle
          strokeWidth: 0.035, // The rotation offset
          color: '#000000' // Fill color
        },
        colorStart: '#6FADCF',   // Colors
        colorStop: '#8FC0DA',    // just experiment with them
        strokeColor: '#E0E0E0',   // to see which ones work best for you
        generateGradient: true
      };
      var target = document.getElementById(attrs.id).getElementsByTagName('canvas')[0]; // your canvas element
      var gauge = new Donut(target).setOptions(opts); // create sexy gauge!
      gauge.maxValue = attrs.max; // set max gauge value
      gauge.set(0);
      //gauge.animationSpeed = attrs.aminspeed; // set animation speed (32 is default value)
      scope.$watch("value", function(value) {
        if(typeof value !== 'undefined') gauge.set(value/10); // set actual value
      });
    }
}});