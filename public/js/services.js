'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
var serviceModule = angular.module('myApp.services', []);

serviceModule.value('version', '0.1')

serviceModule.factory('pubNub', function($rootScope) {
  var pubnub = PUBNUB.init({
    publish_key: 'pub-c-4664659f-7da9-45c8-8655-46c8a77181d0',
    subscribe_key: 'sub-c-57aa8f92-6812-11e2-903d-12313f022c90'
  });

  return pubnub;
});